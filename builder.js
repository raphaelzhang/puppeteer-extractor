const buildSingle = (url, title, html) => {
    return '<!DOCTYPE html>\n' + 
        '<html>\n' + 
        '<head>\n' +
        '    <meta charset="utf-8" />\n' + 
        '    <title>' + title + '</title>\n' +
        '    <style type="text/css" media="all">@import "../img/common.css";</style>\n' + 
        '    <style type="text/css" media="print">@import "../img/print.css";</style>\n' +
        '</head>\n' +
        '<body>\n' +
        '<div id=container>\n' +
        '<div id=header>\n' +
        '    <div class=title>' + title + '</div>\n' +
        '    <div class=nav><a target=_blank href="' + url + '">原网页</a> | <a href="javascript:window.print()">打印</a></div>\n' +
        '</div>\n' +
        '<div id=content>\n' +
        html + 
        '\n</div>\n' +
        '</div>\n' +
        '</body>\n' +
        '</html>';
};

module.exports = {
    buildPage: (url, title, html) => {
        return buildSingle(url, title, html);
    },

    buildFullPdf: function(htmls) {
        let html = '';
        for (let i = 0; i < htmls.length; i++)
            html += '<div' + (i % 2 == 1 ? "" : ' style="background-color:#eee"') + '>\n<div style="text-decoration:none;color:#933">' + htmls[i].name + '</div><br>\n' + htmls[i].content + '</div>\n';
        return '<!DOCTYPE html>\n' +
            '<html>\n' +
            '<head>\n' +
            '    <meta charset="utf-8" />\n' +
            '    <style type="text/css" media="all">@import "../img/common.css";</style>\n' +
            '</head>\n' +
            '<body>\n' +
            '<div id=container>\n' +
            '<div id=content>\n' +
            html +
            '\n</div>\n' +
            '</div>\n' +
            '</body>\n' +
            '</html>';
    },

    buildToc: (url, name, chapters) => {
        let lines = parseInt(chapters.length / 3);
        if (lines * 3 < chapters.length)
            lines++;
             
        let html = '<table width="100%">\n';
        for (let i = 0; i < lines; i++) {
            let tdAttr = (0 == lines) ? ' width="33%"' : '';
            html += '  <tr>\n';
            for (let j = 3*i; j < 3*i+3; j++)
                if (j < chapters.length)
                    html += `    <td${tdAttr}><a href="${(j+1)}.html">${chapters[j].name}</a></td>\n`;
                else
                    html += `    <td${tdAttr}></td>\n`;
            html += '  </tr>\n';
        }
        html += '</table>';
        return buildSingle(url, `${name}/${chapters.length}章`, html);
    },

    buildChapter : (url, title, html, current, maxChapters) => {
        let prev = (current <= 1 ? 'index' : current-1) + '.html',
            next = (current == maxChapters ? 'index' : current+1) + '.html';
        return '<!DOCTYPE html>\n' +
            '<html>\n' +
            '<head>\n' +
            '    <meta charset="utf-8" />\n' +
            '    <title>' + title + '</title>\n' +
            '    <style type="text/css" media="all">@import "../img/novel.css";</style>\n' +
            '</head>\n' +
            '<body>\n' +
            '<div id=container>\n' +
            '<div id=header>\n' +
            '    <div class=title>' + title + '</div>\n' +
            '    <div class=nav><a target=_blank href="' + url + '">原网页</a></div>\n' +
            '</div>\n' +
            '<div id=content_wrap>\n' +
            '<div id=novelnav>\n' +
            '    <a class=toc href="index.html"></a>\n' +
            '    <a class=prevchapter href="' + prev + '"></a>\n' +
            '    <a class=nextchapter href="' + next + '"></a>\n' +
            '    <span>' + current + '</span>\n' +
            '    <span>' + maxChapters + '</span>\n' +
            '</div>\n' +
            '<div id=content>\n' +
            html +
            '\n</div>\n' +
            '</div>\n' +
            '</div>\n' +
            '</body>\n' +
            '<script>\n' +
            'document.body.onkeydown = function(ev) {\n' +
            '    var e = ev || window.event;\n' +
            '    if (e)\n' +
            '    {\n' +
            '        var key = window.event ? e.keyCode : e.which;\n' +
            '        switch (key) {\n' +
            '        case 13: //return\n' +
            '            document.location = "index.html";\n' +
            '            break;\n' +
            '        case 37: //left\n' +
            '            document.location = "' + prev + '";\n' +
            '            break;\n' +
            '        case 39: //right\n' +
            '            document.location = "' + next + '"\n' +
            '            break;\n' +
            '        }\n' +
            '    }\n' +
            '};\n' +
            '</script>\n' +
            '</html>';
    }
};