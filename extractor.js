const extract = (doc) => {
    const minGoodLinkSize = 20000;
    const decorationTags = ["p", "br", "font", "b", "i", "u", "center", "big", "small", "strong", "strike", "sub", "sup", "code", "em", "abbr", "acronym", "address", "blockquote", "cite", "dfn", "samp", "kdb", "var", "del", "ins", "ruby", "rt", "rp", "h1", "h2", "h3", "h4", "h5", "h6", "wbr"];
    const tagsAsWhole = ['tbody', 'thead', 'tfoot', 'th', 'tr', 'td', 'li'];

    const keepBodyOnly = (root) => {
        for (let i = root.childNodes.length-1; i >= 0; i--) {
            const kid = root.childNodes[i];
            if (kid.nodeType != Node.ELEMENT_NODE)
                continue;

            if (kid.tagName.toLowerCase() != 'body')
                kid.remove();
       }
    };

    const calc2D = (root) => {
        root.querySelectorAll('*').forEach(ele => {
            let h = parseInt(getComputedStyle(ele).height), w = parseInt(getComputedStyle(ele).width);
            if (isNaN(h))
                h = 0;
            if (isNaN(w))
                w = 0;
            let oldH = ele.hasAttribute('_h') ? parseInt(ele.getAttribute('_h')) : 0;
            let oldW = ele.hasAttribute('_w') ? parseInt(ele.getAttribute('_w')) : 0;
            
            if (oldH == 0 || oldH > h)
                ele.setAttribute('_h', h);
            if (oldW == 0 || oldW > w)
                ele.setAttribute('_w', w);
        });
    };

    const removeComments = (ele) => {
        for (let i = ele.childNodes.length-1; i >= 0; i--) {
            let node = ele.childNodes[i];
            if(node.nodeType === Node.COMMENT_NODE) {
                node.remove();
            } else {
                removeComments(node);
            }
        }
    };

    const removeEmptyTextNodes = (ele) => {
        for (let i = ele.childNodes.length-1; i >= 0; i--) {
            let node = ele.childNodes[i];
            if(node.nodeType === Node.TEXT_NODE) {
                if (node.textContent.trim().length == 0)
                    node.remove();
            } else {
                removeEmptyTextNodes(node);
            }
        }
    };

    const removeUseless = (root) => {
        // remove comments
        removeComments(root);
        // remove empty text nodes
        removeEmptyTextNodes(root);
        // remove position:absolute elements
        // remove hidden elements
        root.querySelectorAll('*').forEach(ele => {
            let style = getComputedStyle(ele)
            if (style.position == 'absolute' || style.visibility == 'hidden' || style.display == 'none')
                ele.remove();
        });
        const tags = ['script', 'noscript', 'link', 'applet', 'base', 'basefont', 'bgsound', 'button', 'embed', 'frame', 'frameset', 'iframe', 'input', 'isindex', 'label', 'marquee', 'menu', 'object', 'optgroup', 'option', 'param', 'select', 'style', 'textarea'];
        for (let tag of tags)
            root.querySelectorAll(tag).forEach(ele => ele.remove());

        // remove id/class/style/... attrs
        const attrs = ['id', 'class', 'style', 'target'];
        for (let attr of attrs)
            root.querySelectorAll('*').forEach(ele => ele.removeAttribute(attr));
    };

    const calcTextSize = (tn) => {
        const range = doc.createRange();
        range.selectNodeContents(tn);
        const rect = range.getBoundingClientRect();
        return (+rect.height) * (+rect.width);
    };

    const calcSingleSize = (ele) => {
        if (ele.childNodes.length == 0) {
            if (ele.nodeType == Node.ELEMENT_NODE) {
                let w = parseInt(ele.getAttribute('_w'));
                let h = parseInt(ele.getAttribute('_h'));
                ele.setAttribute('_size', w*h);
            }
            return 1;
        }

        let total = 0;
        for (let kid of ele.childNodes) {
            if (kid.nodeType == Node.TEXT_NODE) {
                total += calcTextSize(kid);
            } else if (!kid.hasAttribute('_size')) {
                return -1; // has one kid which has no _size yet
            } else {
                total += parseInt(kid.getAttribute('_size'));
            }
        }

        ele.setAttribute('_size', total);
        return total;
    };

    const calcSize = (root) => {
        while (true) {
            let noSized = root.querySelectorAll('*:not([_size]');
            if (noSized.length == 0)
                break;
            
            noSized.forEach(ele => {
                calcSingleSize(ele);
            });
        }

        for (let kid of root.childNodes)
            calcSingleSize(kid);
    };

    const removeEmpty = (root) => {
        root.querySelectorAll('*[_size="0"]').forEach(ele => {
            const tag = ele.tagName.toLowerCase();
            if (tagsAsWhole.indexOf(tag) == -1 && tag != 'br')
                ele.remove()
        });
        removeEmptyTextNodes(root);
    };

    const calcDensity = (root) => {
        root.querySelectorAll('*').forEach(ele => {
            let effectives = 0;
            ele.querySelectorAll('*').forEach(el => {
                if (decorationTags.indexOf(el.tagName.toLowerCase()) == -1)
                    effectives++;
            });

            const links = ele.querySelectorAll('a');
            let goodLinks = 0;
            links.forEach(el => {
                let size = +(el.getAttribute('_size'));
                return size > minGoodLinkSize;
            });
            
            const count = effectives + links.length - 2*goodLinks + 1;
            const size = +(ele.getAttribute('_size'));
            ele.setAttribute('_children', count);
            ele.setAttribute('_density', size / count);
        });
    };

    const getLinkSize = (ele) => {
        let total = 0;
        ele.querySelectorAll('a').forEach(a => {
            const sz = parseInt(a.getAttribute('_size'));
            total += sz <= minGoodLinkSize ? sz : 0;
        });
        return total;
    };

    const tryParent = (ele) => {
        if (ele.tagName.toLowerCase() == 'body')
            return ele;
        const parent = ele.parentElement;
        if (parent.childNodes.length == 1)
            return tryParent(parent);

        const parentSz = parseInt(parent.getAttribute('_size'));
        const parentLinkSz = getLinkSize(parent);
        const mySz = parseInt(ele.getAttribute('_size'));
        const myLinkSz = getLinkSize(ele);
        const siblingSz = parentSz - mySz;
        const siblingLinkSz = parentLinkSz - myLinkSz;

        // sibLinkSize / sibSize should NOT > 1.5 * (myLinkSize / mySize)
        // i.e, (2 * sls * ms) should be less than (3 * mls * ss)
        if (2 * siblingLinkSz * mySz > 3 * myLinkSz * siblingSz)
            return ele;

        return tryParent(parent);
    };

    const tooManyLinks = (ele) => {
        if (ele.nodeType == Node.TEXT_NODE) // text node
            return false;

        return getLinkSize(ele) * 3 > parseInt(ele.getAttribute('_size'));
    };

    const trimRoot = (ele) => {
        // remove empty text-node
        for (let i = ele.childNodes.length-1; i >= 0; i--)
            if (ele.childNodes[i].nodeType == Node.TEXT_NODE && ele.childNodes[i].textContent.trim().length == 0)
                ele.childNodes[i].remove();

        // trim tail continuously
        for (let i = ele.childNodes.length-1; i >= 0; i--) {
            if (tooManyLinks(ele.childNodes[i]))
                ele.childNodes[i].remove();
            else
                break;
        }

        // trim head continuously
        for (let i = 0; i < ele.childNodes.length; i++) {
            if (tooManyLinks(ele.childNodes[i]))
                ele.childNodes[i].remove();
            else
                break;
        }

        // trim non-decoration elements, but elements such as li/td/tr/thead/tbody/th/tfoot can't be deleted individually
        for (let i = ele.childNodes.length-1; i >= 0; i--) {
            const node = ele.childNodes[i];
            if (node.nodeType == node.TEXT_NODE)
                continue;

            if (decorationTags.indexOf(node.tagName.toLowerCase()) > -1)
                continue;

            if (tooManyLinks(node)) {
                node.remove();
                continue;
            }

            node.querySelectorAll('*').forEach(el => {
                if (tagsAsWhole.indexOf(el.tagName.toLowerCase()) == -1 && tooManyLinks(el))
                    el.remove();
            });
        }

        // trim empty elements except img/td/br
        while(1) {
            let removed = 0;
            ele.querySelectorAll('*').forEach(el => {
                const tag = el.tagName.toLowerCase();
                if ("img" != tag && "br" != tag && tagsAsWhole.indexOf(tag) == -1
                    && el.innerHTML.trim().length == 0) {
                    el.remove();
                    removed++;
                }
            });

            if (removed == 0)
                break;
        }

        // trim mostly empty ul/ol
        ele.querySelectorAll('ul, ol').forEach(el => {
            let empties = 0;
            el.childNodes.forEach(kid => {
                if (kid.innerHTML.trim().length == 0)
                    empties++;
            })
            if (empties*2 > el.childNodes.length)
                el.remove();
        });

        return ele;
    };

    const shrinkSingle = (root, base) => {
        while (root.childNodes.length == 1 && root != base && root.childNodes[0].tagName.toLowerCase() != "pre")
            root = root.childNodes[0];
        return root;
    };

    const selectRoot = (root) => {
        let total = 0;
        root.childNodes.forEach(kid => {
            if (kid.nodeType == Node.ELEMENT_NODE)
                total += parseInt(kid.getAttribute('_size'));
        });

        const bigs = [];
        root.querySelectorAll('*').forEach(ele => {
            const sz = parseInt(ele.getAttribute('_size'));
            if (sz*5 > 2*total)
                bigs.push(ele);
        });
        bigs.sort((a, b) => parseInt(b.getAttribute('_density')) - parseInt(a.getAttribute('_density')));
        if (bigs.length == 0)
            return null;

        const base = bigs[0];
        return trimRoot(shrinkSingle(tryParent(base), base));
    };

    const removeAttrs = (root) => {
        root.querySelectorAll('*').forEach(ele => {
            const attrNames = ele.getAttributeNames();
            for (let an of attrNames)
                if (an.indexOf('_') == 0 || an.indexOf('on') == 0)
                    ele.removeAttribute(an);
        });
    };

    const imgPath = (url) => {
        let pathname = './imgs/' + url.split('://')[1];
        pathname = pathname.replace(/:/gm, "_");
        return pathname;
    }

    let title = doc.title.length == 0 ? doc.location.href : doc.title;
    keepBodyOnly(doc.documentElement);
    calc2D(doc.documentElement);
    removeUseless(doc.documentElement);
    calc2D(doc.documentElement);
    calcSize(doc.documentElement);
    removeEmpty(doc.documentElement);
    calcDensity(doc.documentElement);
    const root = selectRoot(doc.documentElement);
    removeAttrs(doc.documentElement);
    const imgs = [];
    doc.querySelectorAll('img').forEach(img => {
        const pair = {orig: img.src, local: imgPath(img.src)};
        imgs.push(pair);
        img.setAttribute('src', pair.local);
    });
    let html = null == root ? '' : root.innerHTML.replace(/^\s*\n/gm, "");
    return {html: html, title: title, imgs: imgs};
};

module.exports = {
    extractPage: async (page) => {
        const docHandle = await page.evaluateHandle(() => document);
        const result = await page.evaluate(extract, docHandle);
        await docHandle.dispose();
        return result;
    }
};