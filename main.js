'use strict'

const fs = require('fs');
const modPath = require('path');
const modUrl = require('url');
const puppeteer = require('puppeteer');
const indexer = require('./indexer');
const builder = require('./builder');
const extractor = require('./extractor');
const yargs = require('yargs')
          .option('u', {alias: 'url', describe: 'Index page URL', type: 'string'})
          .option('j', {alias: 'javascript', describe: 'Enable javascript', type: 'boolean', default: true})
          .option('s', {alias: 'single', describe: 'Extract single page', type: 'boolean', default: true})
          .option('p', {alias: 'pdf', describe: 'Save novel as one PDF file for phone', type: 'boolean', default: false})
          .option('l', {alias: 'list', describe: 'List local novels', type: 'boolean', default: false})
          .help('h').alias('h', 'help');
const args = yargs.argv;

const forceDir = (dir) => {
  if (fs.existsSync(dir))
    return;

  forceDir(modPath.dirname(dir));
  fs.mkdirSync(dir);
};

const onRequest = async (req) => {
  const parts = modUrl.parse(req.url());
  const adsDomains = ['.google-analytics.com', '.googlesyndication.com', '.doubleclick.net', 'eclick.baidu.com', '.cnzz.com', '.baidustatic.com', 'pos.baidu.com', 'eclick.baidu.com', 'cpro.baidu.com', 'nsclick.baidu.com', 'inte.sogou.com', '.tanx.com', '.revsci.net', 'x.jd.com', '.optimix.asia', '.addthis.com', '.wrating.com', 'qeryz.com'];
  for (let i = 0; i < adsDomains.length; i++) {
    if ((adsDomains[i][0] == '.' && parts.hostname.endsWith(adsDomains[i])) ||
      (adsDomains[i][0] != '.' && parts.hostname == adsDomains[i])) {
      req.abort();
      return;
    }
  }

  if (!args.j && parts.pathname.endsWith('.js')) {
    req.abort();
    return;
  }

  // console.log(`${new Date().toLocaleString()} req: ${req.url()}`);
  req.continue();
};

const onResponse = async (resp) => {
  const ct = resp.headers()['content-type'];
  if (ct == undefined || ct.indexOf('image/') != 0)
    return;

  resp.buffer().then(buf => {
    const url = resp.url();
    const parts = modUrl.parse(url);
    let pathname = `./imgs/${parts.hostname}/${parts.pathname}`;
    pathname = pathname.replace(/:/gm, "_");
    if (!fs.existsSync(pathname)) {
      forceDir(modPath.dirname(pathname));
      fs.writeFileSync(pathname, buf);
    }
  }).catch(err => {});
};

const autoScroll = async (page) => {
  if (!args.j)
    return;

  await page.evaluate(async () => {
    await new Promise((resolve, reject) => {
      let totalHeight = 0;
      const distance = 200;
      const timer = setInterval(() => {
        let scrollHeight = document.body.scrollHeight;
        window.scrollBy(0, distance);
        totalHeight += distance;
        if (totalHeight >= scrollHeight) {
          clearInterval(timer);
          resolve();
        }
      }, 1000);
    });
  });
  console.log(`${new Date().toLocaleString()} scroll done`);
};

const downloadSingle = async (page, url, folder, curChapter, maxChapters) => {
  console.log(`${new Date().toLocaleString()} before downloading page: ${url}`);
  try {
   await page.goto(url, {timeout: 0, waitUntil: 'domcontentloaded'});
  } catch (err) {
    console.log(`${new Date().toLocaleString()} err accessing ${url}, err: ${err}`);
    return false;
  }

  console.log(`${new Date().toLocaleString()} page done: ${url}`);
  await autoScroll(page);
  const result = await extractor.extractPage(page);

  if (curChapter == -1) {
      const domain = modUrl.parse(page.url()).hostname;
      const html = builder.buildPage(url, result.title, result.html);
      fs.writeFileSync(`${folder}/${domain}.html`, html);
    } else {
      const html = builder.buildChapter(url, result.title, result.html, curChapter, maxChapters);
      fs.writeFileSync(`${folder}/${curChapter}.html`, html);
    }
    return true;
};

const fullDocument = async (name, dir, maxChapters) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  let htmls = [];
  for (let i = 1; i <= maxChapters; i++) {
    const buf = fs.readFileSync(`${dir}/${i}.html`, 'utf8');
    await page.setContent(buf);
    const html = await page.evaluate(() => {
      return {name: document.title, content: document.querySelector('#content').innerHTML};
    });
    htmls.push(html)
  }
  const result = builder.buildFullPdf(htmls);
  await page.setContent(result);
  await page.pdf({path: `${dir}/${name}.pdf`, width: 480, height: 640, margin: {top: 10, bottom: 10, left: 15, right: 15}});
  console.log(`${new Date().toLocaleString()} ${dir}/${name}.pdf saved`);

  await browser.close();
};

const downloadNovel = async (page, url) => {
  console.log(`${new Date().toLocaleString()} before loading index page: ${url}`);
  await page.goto(url, {timeout: 0, waitUntil: 'networkidle0'});
  console.log(`${new Date().toLocaleString()} index page loaded: ${url}`);
  const domain = await page.evaluate(() => document.domain);

  const name = await indexer.getPageNovelName(page);
  console.log(`${new Date().toLocaleString()} novel name: ${name}`);
  const chapters = await indexer.getPageChapterLinks(page);

  const indexHtml = builder.buildToc(url, name, chapters);
  const dir = `./output/${name}_${domain}`;
  forceDir(dir);
  fs.writeFileSync(`${dir}/index.html`, indexHtml);

  let failed = [];
  for (let i = 1; i <= chapters.length; i++) {
    const filename = `${dir}/${i}.html`;
    if (fs.existsSync(filename))
      continue;
    console.log(`${new Date().toLocaleString()} ${i}/${chapters.length}`);
    let ok = await downloadSingle(page, chapters[i-1].link, dir, i, chapters.length);
    if (!ok)
      failed.push(chapters[i-1]);
  }

  if (failed.length > 0) {
    console.log('failed:');
    for (let f of failed)
      console.log(`\tname: ${f.name}, link: ${f.link}`);
  } else if (args.pdf) {
    fullDocument(name, dir, chapters.length);
  }
};

const listNovels = async () => {
  const novels = [];
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  for (let sub of fs.readdirSync('./output')) {
    if (sub.indexOf('_') == -1)
      continue;

    const folderStat = fs.statSync(`./output/${sub}`);
    if (!folderStat.isDirectory())
      continue;

    const fn = `./output/${sub}/index.html`;
    if (!fs.existsSync(fn))
      continue;

    const fileStat = fs.statSync(fn);
    if (!fileStat.isFile)
      continue;

    const buf = fs.readFileSync(fn, 'utf8');
    await page.setContent(buf);
    const title = (await page.title()).split('/');
    const name = title[0];
    const chapters = title[1];
    const url = await page.evaluate(() => document.querySelector('#header .nav a').href);
    novels.push({name: name, mtime: fileStat.mtimeMs, chapters: chapters, url: url});
  }

  novels.sort((a, b) => {
    return a.mtime - b.mtime;
  });

  const minText = (text, len) => {
    const orig = text.length;
    for (let i = orig*2; i < len; i++)
      text += ' ';
    return text;
  };
  for (let novel of novels) {
    console.log(`${new Date(novel.mtime).toLocaleString()}\t${minText(novel.name, 20)}${novel.chapters}\t${novel.url}`);
  }

  await browser.close();
};

if (!args.l && args.url == undefined) {
  yargs.showHelp();
  process.exit(1);
} else if (args.l) {
  listNovels();
} else {
  puppeteer.launch().then(async browser => {
    const page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36');
    await page.setViewport({width: 1440, height: 2560});
    await page.setRequestInterception(true);
    page.on('console', msg => console.log(msg.text()));
    page.on('request', onRequest);
    page.on('response', onResponse);

    if (!args.j)
      await page.setJavaScriptEnabled(false);

    if (args.s) {
      await downloadSingle(page, args.url, './output', -1, -1);
    } else {
      await downloadNovel(page, args.url);
    }
    await browser.close();
  });
}