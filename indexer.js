const getNovelName = (doc) => {
    let name = '';
    const links = doc.querySelectorAll(`a[href="${doc.location.href}"]`);

    if (links.length > 0) {
        name = links[0].innerText.trim();
        if (name.length > 0)
            return name;
    }

    const keywords = doc.querySelector('meta[name="keywords"]').getAttribute('content').replace(/[,\|\/ \-_》《、]/g, '|').split(/\|/g);
    const parts = keywords.concat(doc.title.replace(/[,\|\/ \-_》《、]/g, '|').split(/\|/g));
    console.log(`parts[${parts.length}]: ` + parts.join(' | '));
    for (let i = 2; i <= parts[0].length; i++) {
        let word = parts[0].substr(0, i);
        let match = 0, miss = 0;
        for (let j = 1; j < parts.length; j++) {
            if (parts[j].indexOf(word) > -1)
                match++;
            else
                miss++;
        }
        console.log(`word: ${word}, match: ${match}, miss: ${miss}`);
        if (match == 0) {
            name = parts[0].substr(0, i-1);
            break;
        }
    }
    return name.length == 0 ? parts[0] : name;
};

const getChapterLinks = (doc) => {
    let parentPath = doc.location.pathname;
    parentPath = parentPath.substring(parentPath.lastIndexOf('/')+1, -1);
    console.log(`parent-path: ${parentPath}`);

    const links = [];
    doc.querySelectorAll('a[href]').forEach(a => {
        if (a.pathname.indexOf(parentPath) == 0 && a.pathname != parentPath)
            links.push({link: a.href, name: a.innerHTML});
    });
    links.sort((a, b) => {
        if (a.link.length == b.link.length)
            return a.link.localeCompare(b.link);
        else
            return a.link.length < b.link.length ? -1 : 1;
    });

    const chapters = [];
    links.forEach(a => {
        let uniq = true;
        for (let i = chapters.length-1; i >= 0; i--) {
            if (chapters[i].link == a.link || chapters[i].name == a.name) {
                uniq = false;
                break;
            }
        }

        if (uniq)
            chapters.push(a);
    });
    return chapters;
};

module.exports = {
    getPageNovelName: async (page) => {
        const docHandle = await page.evaluateHandle(() => document);
        const name = await page.evaluate(getNovelName, docHandle);
        await docHandle.dispose();
        return name;
    },

    getPageChapterLinks: async (page) => {
        const docHandle = await page.evaluateHandle(() => document);
        const chapters = await page.evaluate(getChapterLinks, docHandle);
        await docHandle.dispose();
        return chapters;
    }
};