'use strict'

const pptr = require('puppeteer');
const args = require('yargs')
          .option('u', {alias: 'url', demand: true, describe: 'wenku page URL', type: 'string'})
          .help('h').alias('h', 'help').argv;

const autoScroll = async (page) => {
  await page.evaluate(async () => {
    await new Promise((resolve, reject) => {
      let totalHeight = 0;
      const distance = 300;
      const timer = setInterval(() => {
        let scrollHeight = document.body.scrollHeight;
        window.scrollBy(0, distance);
        totalHeight += distance;
        console.log(`th: ${totalHeight}, sh: ${document.body.scrollHeight}`);
        if (totalHeight >= scrollHeight) {
          clearInterval(timer);
          resolve();
        }
      }, 200);
    });
  });
  console.log(new Date().toLocaleString() + ' scroll done');
};

const screenshotDOMElement = async (page, selector, filename) => {
  await page.evaluate(selector => document.querySelector(selector).scrollIntoView(), selector);

  await page.waitForSelector(selector);
  await page.waitFor(10000);
  // await page.waitForNavigation({waitUntil: 'networkidle2'});

  const rect = await page.evaluate(selector => {
    const element = document.querySelector(selector);
    const {x, y, width, height} = element.getBoundingClientRect();
    return {left: x, top: y, width, height};
  }, selector);

  return await page.screenshot({
    path: filename,
    clip: {
      x: rect.left,
      y: rect.top,
      width: rect.width,
      height: rect.height
    }
  });
}

// in chrome:
// 1. open devtool (F12)
// 2. select one element and scroll into view (using menu)
// 3. ctrl-shift-p to open command panel
// 4. input capture node screenshot
pptr.launch({headless: false}).then(async (browser) => {
    const page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36');
    await page.setViewport({width: 1280, height: 1920});
    page.on('console', msg => console.log(msg.text()));

    await page.goto(args.url, {waitUntil: 'domcontentloaded'});

    /*
    await page.evaluate(() => {
        document.querySelectorAll('.aside').forEach(ele => ele.remove());
        document.querySelectorAll('iframe').forEach(ele => ele.remove());
        document.querySelector('#hd').remove();
        document.querySelector('body > div.wk-other-new-cntent').remove();
        document.querySelector('#doc-header-test').remove();
        document.querySelector('#bd > div > div > div.crubms-wrap').remove();
        document.querySelectorAll('#doc-main > div > div.ft').forEach(ele => ele.remove());
        document.querySelectorAll('body > div.fix-searchbar-wrap').forEach(ele => ele.remove());
        document.querySelector('#ft').remove();
        document.querySelectorAll('body > div.reader-tools-bar-wrap.tools-bar-small.tools-bar-smaller').forEach(ele => ele.remove());
    });
    */

    await page.click('#html-reader-go-more > div.continue-to-read > div.banner-more-btn > span > span.fc2e');
    let pageNo = 1;
    while (true) {
      const pg = await page.$(`#pageNo-${pageNo}`);
      if (pg == null) {
        console.log(`max-page: ${pageNo-1}`)
        break;
      }
      console.log(`${new Date().toLocaleString()} before screenshot page ${pageNo}`);
      await screenshotDOMElement(page, `#pageNo-${pageNo}`, `wenku-p${pageNo}.png`);
      pageNo++;
      if (pageNo > 5)
        break;
    }
    // await page.click('#reader-container-1 > a');
    // await autoScroll(page);
    // await page.screenshot({path: 'wenku.png'});
    // await page.pdf({path: 'wenku.pdf'});
    // console.log('pdf saved');
    // await browser.close();
});