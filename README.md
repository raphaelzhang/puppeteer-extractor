# 基于puppeteer的正文提取

## 安装

* 先安装[nodejs](https://nodejs.org/en/)
* 再安装puppeteer与yargs，并配置
    * `npm i puppeteer -g` 再 `npm link puppeteer`
    * `npm i yargs -g` 再 `npm link yargs`
    * 不然需要设置 `NODE_PATH` 环境变量，如 `set NODE_PATH=%AppData%\npm\node_modules`

## 使用

* `node main.js -h` 查看帮助
* 下载小说： `node main.js -j false -s false -u $NOVEL_INDEX_URL`
* 下载单页： `node main.js -u $PAGE_URL`

## TODO

* 生成单页html/text